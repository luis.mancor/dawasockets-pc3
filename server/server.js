const express = require('express');
const socketIO = require('socket.io');
const http = require('http');
const path = require('path');

//lab13
const {crearMensaje} = require("./utilidades/utilidades");

const {Usuarios} = require("./classes/usuarios");
const { callbackify } = require('util');
//const { callbackify } = require('util');
const app = express();

let server = http.createServer(app);
const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));

let io = socketIO(server);


//Lab 13
const usuarios = new Usuarios();

io.on('connection', (client) => {
    console.log("Usuario contectado")
    /*
    client.on("entrarChat", (usuario) => 
        console.log("Usuario conectado ", usuario)
    );
    */
    client.on('entrarChat', (data, callback )=> {
        if (!data.nombre) {
            return callback({
                error: true,
                mensaje: 'El nombre/sala es necesario'
            });
        }
        let personas = usuarios.agregarPersona(client.id,data.nombre);
        callback(personas);    
    });

    client.on("crearMensaje", (data) => {
        let persona = usuarios.getPersona(client.id);
        let mensaje = crearMensaje(persona.nombre, data.mensaje);
        client.broadcast.emit("crearMensaje", mensaje);
    });

    client.on("disconnect", () =>{
        let personaBorrada = usuarios.borrarPersona(client.id);

        client.broadcast.emit("crearMensaje",{
            usuario: "Administrador",
            mensaje: crearMensaje('Admin', `${personaBorrada.nombre} salio` ),
        });

        client.broadcast.emit("listaPersonas", usuarios.getPersonas());

    });

});






//server.listen(app.get(port));

server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${ port }`);

});