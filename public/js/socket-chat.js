var socket = io();

var params = new URLSearchParams(window.location.search);

if(!params.has("nombre")){
    window.location = "index.html";
    throw new Error("El nombre es necesario");
}

var usuario = {
    nombre : params.get("nombre"),
};

socket.on("connect", function(){
    console.log("Conectado al servidor");

    socket.emit("entrarChat", usuario, function(resp){
        console.log("Usuarios conectados", resp);
    });
});

socket.on("disconnect", function(){
    console.log("Perdimos conexion con el servidor");
});

socket.on("crearMensaje", function(mensaje){
    console.log("Servidor mensaje: ", mensaje);
});

socket.on("listaPersonas", function(personas){
    console.log(personas);
});
